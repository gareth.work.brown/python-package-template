# Francesco's python package template

Want to create a new python package starting from my template?
Be sure to have installed [cookiecutter](https://cookiecutter.readthedocs.io) and run:
```shell
cookiecutter https://gitlab.com/francesco-calcavecchia/francesco-python-package-template
```


## What does this template contain?

* An initialized git repo
* A python virtualenv in the folder `venv` with [`pre-commit`](https://pre-commit.com/) already installed
* git [pre-commit](https://pre-commit.com/) installed with several linters and formatter such as `isort`, `black`, `mypy`, `pylint`, `refurb` and `flake8`, that will check your code quality right before commits
* All setup necessary to build a python package, with version taken grom git using [setuptools-scm](https://pypi.org/project/setuptools-scm/)
* A `.gitlab-ci.yml` that will:
  * run tests on all commits
  * build and push the package into the GitLab pypi repository as a reaction for a tag (typically for a new release)
* A changelog file
* A minimal readme file. Just follow the TODO to know how to go on. Don't follow "Setup development environment", it has already been done by cookiecutter


## What does this template not contain?

* Documentation setup
* Advanced testing setup (use bare bones [`unittest`](https://docs.python.org/3/library/unittest.html))
* 
