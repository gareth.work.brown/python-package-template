#!/bin/sh

echo $(pwd)

git init

python -m venv venv
. venv/bin/activate
python -m pip install -U pip wheel setuptools
python -m pip install -r requirements-dev.txt

pre-commit install
