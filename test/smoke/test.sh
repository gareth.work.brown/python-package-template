pushd my-package-dir

if [ ! -f .git/hooks/pre-commit ]; then
    exit 1
fi

python -m unittest discover test

popd
