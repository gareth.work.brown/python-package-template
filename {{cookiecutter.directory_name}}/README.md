# {{cookiecutter.package_name}}
{{cookiecutter.package_short_description}}


## TODO: how to go on after creating the package
- add the required packages to `pyproject.toml` in the field `dependencies`
- complete filling the `pyproject.toml` with the optional information commented out
- the built python package will be pushed by default to the GitLab pypi repository. If you want to change this behaviour, modify the task `deploy package` in the file `.gitlab-ci.yml`.


## Setup development environment (for contributors only)

* Create a virtual environment and activate it (if you have [pyenv](https://github.com/pyenv/pyenv) installed the 
  version of python will be automatically set, otherwise refer to the file `.python-version`)
  ```shell
  python -m venv venv
  source venv/bin/activate
  ```

* Install the developer dependencies you will need
  ```shell
  pip install -U pip wheel setuptools
  pip install requirements-dev.txt
  ```
  
* Enable the pre-commits
  ```shell
  pre-commit install
  ```
  
* To run the tests
  ```shell
  python -m unittest discover test
  ```
